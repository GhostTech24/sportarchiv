var model = (function () { // encapsulation / Kapselung

    var appDb = null; // the indexedDB we will use
    var memberList = new Map();
    var lastMemberId = 1;
    var freeIds = [];
    var contributionsList = new Map();
    const memberListStorekey = "memberList";
    const contributionsListStorekey = "contributionsList";
    const objectStoreName = 'storedValues';

    /**
     * eng: Validator of the PoC. Normally this should be a single Class or javascript-file, but for this PoC its enough.
     * ger: Validator des PoC. Normalerweise sollte dies eine eigene Klasse oder Javascript-Datei sein, aber fuer diesen PoC ist es genug. 
     */
    var validator = {
        /**
         * eng: Validator function. Returns the boolean value, if the browser support the indexDB technology currently.
         * ger: Validierungsfunktion. Gibt den boolschen Wert zurueck, ob der Browser aktuell die IndexDB-Technologie unterstuetzt.
         */
        isIndexDBSupported: function() {
            if (window.indexedDB) {
                return true;
            } else {
                console.error('Der Browser unterstuetzt aktuell nicht die Technologie IndexDB!');
                return false;
            }
        }
    };

    openDb = function() {
        const DB_NAME = 'MyDb';
        const DB_VERSION = '1';

        if(validator.isIndexDBSupported)  {
            return new Promise((resolve, reject) => {
                let openDbRequest
                try {
                    
                    openDbRequest = window.indexedDB.open(DB_NAME, DB_VERSION); // async function.

                    openDbRequest.onerror = function() { // error
                        const errorMessagePart = openDbRequest.error.toString().slice(0,17);
                        const knownError = 'InvalidStateError'
                        const errorMessagePrivateOrNoChronic = 'Die IndexedDB wurde nicht geöffnet! Das kann mehrere Gründe haben, z.B: Privater Modus im Browser, keine Chronik im Browser.'
                    
                        let shownMessage

                        console.error('Ergebnis war: |' + errorMessagePart)

                        if(knownError === errorMessagePart) {
                            shownMessage = openDbRequest.error + '\n' + errorMessagePrivateOrNoChronic;
                        } else {
                            shownMessage = openDbRequest.error;
                        }

                        return reject(shownMessage)
                    };

                    openDbRequest.onsuccess = function() { // every successful (except first)
                        return resolve(openDbRequest.result)
                    };

                    openDbRequest.onupgradeneeded = function() { // only first successfull; To trigger this event again, delete the offline-data!
                        const db = openDbRequest.result;
                        const objectStore = db.createObjectStore(objectStoreName); // no path, no key gen

                        console.log('Store angelegt. Instance: ' + objectStore);

                        objectStore.transaction.oncomplete = function() {
                            var exampleMemberlist = new Map(); 

                            exampleMemberlist.set(1, new Member('Demoman', 'First', 'm', new Date('01/01/2000'), new Date('01/01/2018'), 'Beispielstraße', '1', '56626', 'Andernach', 'GER', 'Softwaretester', null, null, 'firstDemoman@hotmail.com', false, false, 'Fortgeschrittener', 'Diabetiker'));
                            exampleMemberlist.set(2, new Member('Demowoman', 'Second', 'w', new Date('02/02/2000'), new Date('02/02/2018'), 'Beispielstr.', '2b', '56575', 'Weißenthurm', 'GER', 'Verkäuferin', null, '02637123456', null, false, false, 'Fortgeschrittener', ''));
                            var myObjectStore = db.transaction([objectStoreName], "readwrite").objectStore(objectStoreName);
                            
                            myObjectStore.add(exampleMemberlist, memberListStorekey);      
                            
                            console.log('Store erfolgreich zum ersten mal eingerichtet! (mit Werten)')
                            resolve(openDbRequest.result)      
                        }
                    }
                } catch (errorMessage) {
                    const errorMessagePart = errorMessage.toString().slice(0,13);
                    const knownError = 'SecurityError'
                    const errorMessageCookies = 'Firefox wird ohne Cookies ausgeführt (Cookies von Drittseiten blockiert)! DB kann nicht geöffnet werden! siehe dazu: https://github.com/Modernizr/Modernizr/issues/1825 (Stand 2020.09.25)'
                    let shownMessage

                    if(knownError === errorMessagePart) {
                        shownMessage = errorMessage + '\n' + errorMessageCookies;
                    } else {
                        shownMessage = errorMessage;
                    }

                    reject(shownMessage);                   
                }
            }); 
        }
    };

    saveData = function(appDb, storekey, value){
        return new Promise((resolve, reject) => {
            const readTransaction = appDb.transaction([objectStoreName], "readwrite") // Access to DB
            const targetObjectStore = readTransaction.objectStore(objectStoreName) // Access to ObjectStore
            const resultOfRead = targetObjectStore.get(storekey)

            resultOfRead.onerror = function() {
                reject(resultOfRead.errorCode)
            };

            resultOfRead.onsuccess = function() { 
                var itemWithWriteAccess = resultOfRead.result

                itemWithWriteAccess = value; // here happens the update on the item

                var resultOfWrite = targetObjectStore.put(itemWithWriteAccess, storekey) // save changed item on the store
                resultOfWrite.onerror = function(event) {
                    reject(resultOfRead.errorCode)
                };
                resultOfWrite.onsuccess = function(event) {
                    resolve(resultOfRead.result)
                };  
            };
        });
    };

    loadData = function(appDb, storekey) {
        return new Promise((resolve, reject) => {
            const readTransaction = appDb.transaction([objectStoreName]) // Access to DB
            const targetObjectStore = readTransaction.objectStore(objectStoreName) // Access to ObjectStore
            const resultOfRead = targetObjectStore.get(storekey)

            resultOfRead.onerror = function() {
                console.log('Es gab einen Fehler bei de Anfrage: ' + resultOfRead.errorCode)
                reject(resultOfRead.errorCode)
            };

            resultOfRead.onsuccess = function() {            
                resolve(resultOfRead.result) // alternativ: event.target.result wenn in function event ist
            };
        });
    };

    // public API / oeffentliche API
    return {      
        initMemberList: function() {
            contributionsList = new Map();
            memberList = new Map();
            lastMemberId = 1;
            freeIds = [];
        },

        updateMemberList: async function() {
            const newMemberList = this.getMemberList();
            
            const appDb = await openDb();
            await saveData(appDb, memberListStorekey, newMemberList);
        },

        loadMemberList: async function() {
            this.initMemberList();
            const appDb = await openDb();
            const loadedData = await loadData(appDb, memberListStorekey)

            for(var [key, member] of loadedData){
                memberList.set(key, member)
            }
        },
 
        getStructure: function() { //TODO maybe put this function into the frontend
            var structure = [];
            var ageborders = [7,15,19,27,41,61]
            var birthdayborders = [];
            birthdayborders.push(new Date((new Date().getMonth()+ 1) +'/'+new Date().getDate()+'/'+ (new Date().getFullYear()-ageborders[0])))
            birthdayborders.push(new Date((new Date().getMonth()+ 1) +'/'+new Date().getDate()+'/'+ (new Date().getFullYear()-ageborders[1])))
            birthdayborders.push(new Date((new Date().getMonth()+ 1) +'/'+new Date().getDate()+'/'+ (new Date().getFullYear()-ageborders[2])))
            birthdayborders.push(new Date((new Date().getMonth()+ 1) +'/'+new Date().getDate()+'/'+ (new Date().getFullYear()-ageborders[3])))
            birthdayborders.push(new Date((new Date().getMonth()+ 1) +'/'+new Date().getDate()+'/'+ (new Date().getFullYear()-ageborders[4])))
            birthdayborders.push(new Date((new Date().getMonth()+ 1) +'/'+new Date().getDate()+'/'+ (new Date().getFullYear()-ageborders[5])))
            var malegroup =[0,0,0,0,0,0,0];
            var femalegroup =[0,0,0,0,0,0,0];
            var diversegroup =[0,0,0,0,0,0,0];

            for(var [key, member] of memberList.entries()){
                if (member.gender === 'm'){ 
                    if (member.dateOfBirth > birthdayborders[0]){ malegroup[0] = malegroup[0]+1; }
                    if (member.dateOfBirth < birthdayborders[0] && member.dateOfBirth >= birthdayborders[1]){ malegroup[1] = malegroup[1]+1; }
                    if (member.dateOfBirth < birthdayborders[1] && member.dateOfBirth >= birthdayborders[2]){ malegroup[2] = malegroup[2]+1; }
                    if (member.dateOfBirth < birthdayborders[2] && member.dateOfBirth >= birthdayborders[3]){ malegroup[3] = malegroup[3]+1; }
                    if (member.dateOfBirth < birthdayborders[3] && member.dateOfBirth >= birthdayborders[4]){ malegroup[4] = malegroup[4]+1; }
                    if (member.dateOfBirth < birthdayborders[4] && member.dateOfBirth >= birthdayborders[5]){ malegroup[5] = malegroup[5]+1; }
                    if (member.dateOfBirth < birthdayborders[5]){ malegroup[6] = malegroup[6]+1; }
                }
                if (member.gender === 'f'){ 
                    if (member.dateOfBirth > birthdayborders[0]){ malegroup[0] = femalegroup[0]+1; }
                    if (member.dateOfBirth < birthdayborders[0] && member.dateOfBirth >= birthdayborders[1]){ femalegroup[1] = femalegroup[1]+1; }
                    if (member.dateOfBirth < birthdayborders[1] && member.dateOfBirth >= birthdayborders[2]){ femalegroup[2] = femalegroup[2]+1; }
                    if (member.dateOfBirth < birthdayborders[2] && member.dateOfBirth >= birthdayborders[3]){ femalegroup[3] = femalegroup[3]+1; }
                    if (member.dateOfBirth < birthdayborders[3] && member.dateOfBirth >= birthdayborders[4]){ femalegroup[4] = femalegroup[4]+1; }
                    if (member.dateOfBirth < birthdayborders[4] && member.dateOfBirth >= birthdayborders[5]){ femalegroup[5] = femalegroup[5]+1; }
                    if (member.dateOfBirth < birthdayborders[5]){ malegroup[6] = femalegroup[6]+1; }
                } 
                if (member.gender === 'd'){ 
                    if (member.dateOfBirth > birthdayborders[0]){ malegroup[0] = diversegroup[0]+1; }
                    if (member.dateOfBirth < birthdayborders[0] && member.dateOfBirth >= birthdayborders[1]){ diversegroup[1] = diversegroup[1]+1; }
                    if (member.dateOfBirth < birthdayborders[1] && member.dateOfBirth >= birthdayborders[2]){ diversegroup[2] = diversegroup[2]+1; }
                    if (member.dateOfBirth < birthdayborders[2] && member.dateOfBirth >= birthdayborders[3]){ diversegroup[3] = diversegroup[3]+1; }
                    if (member.dateOfBirth < birthdayborders[3] && member.dateOfBirth >= birthdayborders[4]){ diversegroup[4] = diversegroup[4]+1; }
                    if (member.dateOfBirth < birthdayborders[4] && member.dateOfBirth >= birthdayborders[5]){ diversegroup[5] = diversegroup[5]+1; }
                    if (member.dateOfBirth < birthdayborders[5]){ malegroup[6] = diversegroup[6]+1; }
                } 
            }
            structure = {'m': malegroup, 'f':femalegroup, 'd': diversegroup, 'ageborders': ageborders};
            return structure;
        },
        
        deleteMember: function() {
            var deleteAfterYears = this.getDeleteYear();
            var listsizebefore = memberList.size;
            for(var [key, member] of memberList.entries()){
                if (member.dateOfLeaving instanceof Date){ 
                    if (member.dateOfLeaving <= new Date((new Date().getMonth()+1) +'/'+new Date().getDate() + '/'+ (new Date().getFullYear()-deleteAfterYears))){
                        //deletes both member and his contributions
                        memberList.delete(key);
                        contributionsList.delete(key);
                    }
                }
            }
            console.log('Es wurde auf zu löschende Mitglieder geprüft. Anzahl gelöschter Mitglieder: |' + (listsizebefore-memberList.size)+'|')
            return true;
        },

        getDeleteYear: function() {
            const deleteAfterYears = 6;
            return deleteAfterYears;
        },

        addMember: function(lastname, firstname, gender, dateOfBirth, dateOfAccession, streetName, streetNumber, postalCode, residence, nationality, job, mobileNumber, landlineNumber, email, isExecutive, isTrainer, rank, comment) {
            var isValid, newMember, choosenId

            newMember = new Member(lastname, firstname, gender, dateOfBirth, dateOfAccession, streetName, streetNumber, postalCode, residence, nationality, job, mobileNumber, landlineNumber, email, isExecutive, isTrainer, rank, comment);

            isValid = newMember.isValidMember();

            if(isValid){
                
                if(freeIds.length !== 0) {
                    choosenId = freeIds[0];
                    freeIds.shift();
                } else {
                    choosenId = lastMemberId;
                    lastMemberId++;
                }
                memberList.set(choosenId, newMember)
                this.addContribution(choosenId)
                return true;
            } else {
                var messageErrorValidation = 'Das aufzunehmende Mitglied ist ungültig! Prüfen sie erneut die Eingabe.'
                console.log(messageErrorValidation)
                // TODO alert(messageErrorValidation) // ins Frontend!
                return false;
            }
        },
            
        addManyMembers: function(manyMembers) { //TODO edit this function so it can read an excel sheet, maybe put this function into the frontend
            for(var [key, member] of manyMembers){
                model.addMember(member.lastname, member.firstname, member.gender, member.dateOfBirth, member.dateOfAccession, member.streetName, member.streetNumber, member.postalCode, member.residence, member.nationality, member.job, member.mobileNumber, member.landlineNumber, member.email, member.isExecutive, member.isTrainer, member.rank, member.comment);
            }
        },

        changeMember: function(id, changedMember) {
            var isValid

            isValid = changedMember.isValidMember()

            if(isValid) {
                memberList.set(id, changedMember)
                return true;
            } else {
                const errorMessage = 'Das zu ändernde Mitglied ist ungültig! Prüfen sie erneut die Eingabe.'
                console.log(errorMessage)
                return false;
            }
        },

        getMemberList: function () {
            return memberList;
        },

        //
        //CONTRIBUTIONFUNCTIONS, //TODO SOME OF THESE SHOULD BE PUT OUTSIDE OF CAPSULATION
        //
        //TODO this function, it should only be private and called by the addMember function
        addContribution : function(id) {
            var isFullAge, paymentStatus //TODO add a method to implement a comment for the contribution list
            paymentStatus = new Map();
            if (memberList.get(id).dateOfBirth <= this.getCurrentDateOfMaturity()) {
                isFullAge = true;
            } else { 
                isFullAge = false 
            }
            newContribution = new contribution(isFullAge, paymentStatus, '');
            contributionsList.set(id, newContribution)
            return true;
        },

        getCurrentDateOfMaturity : function(){
            const ageOfMaturity = 18;
            var currentDateOfMaturity

            currentDateOfMaturity = new Date(new Date().getFullYear()-ageOfMaturity, new Date().getMonth(), new Date().getDate()); 
            return currentDateOfMaturity;
        },

        getContributionsList: function(){
            return contributionsList;
        },


        // TODO this function should also only be private, when does the program check for it? Beginning of the Year? (Stable contribution) Every Month?
        checkMaturity : function(id){
            if(memberList.get(id).dateOfBirth <= this.currentDateOfMaturity()) {
                contributionsList.set(id).set(true);
            } else { contributionsList.set(id).set(false) }
        },

        setPaymentStatus : function(id, year, paymentArray){
            contributionsList.get(id).paymentStatus.set(year, paymentArray)
            this.checkMaturity
        },
      
        getPaymentExplanation : function (){
            var explanations = new Map();
            explanations.set(1, 'Not a member')
            explanations.set(2, 'Full age member')
            explanations.set(3, 'Underage member')
            explanations.set(4, 'Trainer and/or CEO')
            explanations.set(5, 'Cancelled membership')
            explanations.set(6, 'Inactive')
            return explanations;
        },

        updateContributionList: async function() {
            const newcontributionsList = this.getContributionsList();
            
            const appDb = await openDb();
            await saveData(appDb, contributionsListStorekey, newcontributionsList);
        },

        loadContributionList: async function() {
            this.initMemberList();
            const appDb = await openDb();
            const loadedData = await loadData(appDb, contributionsListStorekey)

            for(var [key, contribution] of loadedData){
                contributionsList.set(key, contribution)
            }
        }
        //
        //
        //CONTRIBUTIONFUNCTIONS END
        //
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function() {
    console.log('Model-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)
