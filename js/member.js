class Member {
    
    constructor(lastname, firstname, gender, dateOfBirth, dateOfAccession, streetName, streetNumber, postalCode, residence, nationality, job, mobileNumber, landlineNumber, email, isExecutive, isTrainer, rank, comment) {
        this.lastname = lastname,
        this.firstname= firstname,
        this.gender= gender.toString().toLowerCase(),
        this.dateOfBirth= dateOfBirth,
        this.dateOfAccession= dateOfAccession,
        this.dateOfLeaving = null,
        this.streetName= streetName,
        this.streetNumber= streetNumber,
        this.postalCode= postalCode,
        this.residence= residence,
        this.nationality= nationality,
        this.job= job,
        this.mobileNumber= mobileNumber,
        this.landlineNumber= landlineNumber,
        this.email= email,
        this.isExecutive= isExecutive,
        this.isInactive= false,
        this.isTrainer= isTrainer,
        this.rank= rank,
        this.comment= comment
    }


    isValidLastname() {
        if(this.lastname === null || this.lastname === undefined || this.lastname.length === 0 || typeof(this.lastname) !== 'string') { 
            console.log('Der Nachname ist ungültig. Aktueller Wert: |' + this.lastname + '| vom Typ |' + typeof(this.lastname) + '|'); 
            return false; 
        }
        return true;
    }

    isValidFirstname() {
        if(this.firstname === null || this.firstname === undefined || this.firstname === 0 || typeof(this.firstname) !== 'string') { 
            console.log('Der Vorname ist ungültig. Aktueller Wert: |' + this.firstname + '| vom Typ |' + typeof(this.firstname) + '|'); 
            return false; 
        }
        return true;
    }

    isValidGender() {
        if(this.gender !== 'm' && this.gender !== 'w' && this.gender !== 'd') {
            console.log('Das Geschlecht ist ungültig. Aktueller Wert: |' + this.gender + '| vom Typ |' + typeof(this.gender) + '|');
            return false; 
        }
        if(typeof(this.gender) !== 'string') {
            console.log('Das Geschlecht ist ungültig. Aktueller Wert: |' + this.gender + '| vom Typ |' + typeof(this.gender) + '|');
            return false;
        }
        return true;
    }

    isValiddateOfBirth() {
        const dateFrom = "01/01/1900";
        const minage = 2;
        var dateTo;
        var d1;
        var d2;
        var c;
        var from;  
        var to;
        if(this.dateOfBirth === null || this.dateOfBirth === undefined) { console.log('Das Geburtsdatum ist ungültig. Aktueller Wert: |' + this.dateOfBirth + '| vom Typ |' + typeof(this.dateOfBirth) + '|'); return false; } 
        if (!(this.dateOfBirth instanceof Date)) { console.log('Das Geburtsdatum ist ungültig. Aktueller Wert: |' + this.dateOfBirth + '| vom Typ |' + typeof(this.dateOfBirth) + '|'); return false; } 
        dateTo = new Date().getDate()+'/'+ (new Date().getMonth()+ 1) +'/'+(new Date().getFullYear()-minage); // The latest valid birthdate 
        d1 = dateFrom.split("/");
        d2 = dateTo.split("/");
        c = [this.dateOfBirth.getDate(),this.dateOfBirth.getMonth()+1,this.dateOfBirth.getFullYear()]
        from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
        to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
        if (this.dateOfBirth < from || this.dateOfBirth > to) { console.log('Das Geburtsjahr ist ungültig. Aktueller Wert: |' + this.dateOfBirth + '| vom Typ |' + typeof(this.dateOfBirth) + '|'); return false; } 
        if (c[0] < 1 || c[0] > 31) { console.log('Der Geburtstag ist ungültig. Aktueller Wert: |' + c[0] + '| vom Typ |' + c[0] + '|'); return false; }
        if (c[1] < 1 || c[1] > 12) { console.log('Der Geburtsmonat ist ungültig. Aktueller Wert: |' + c[1] + '| vom Typ |' + c[1] + '|'); return false; }  
        if (c[2] < d1[2] || c[2] > d2[2]) { console.log('Das Geburtsjahr ist ungültig. Aktueller Wert: |' + c[2] + '| vom Typ |' + c[2] + '|'); return false; }

        return true;
    }

    isValiddateOfAccession() {
        const dateFrom = "01/01/1900";
        var d1;
        var c;
        if(this.dateOfAccession === null || this.dateOfAccession === undefined) { console.log('Das Geschlecht ist ungültig. Aktueller Wert: |' + this.dateofAccession + '| vom Typ |' + typeof(this.dateofAccession) + '|'); return false; } 
        if (!(this.dateOfAccession instanceof Date)) { console.log('Das Datumsformat ist ungültig. Aktueller Wert: |' + this.dateofAccession + '| vom Typ |' + typeof(this.dateofAccession) + '|'); return false; } 
        d1 = dateFrom.split("/"); 
        c = [this.dateOfAccession.getDate(),this.dateOfAccession.getMonth()+1,this.dateOfAccession.getFullYear()]
        if (c[0] < 1 || c[0] > 31) { console.log('Der Beitrittstag ist ungültig. Aktueller Wert: |' + c[0] + '| vom Typ |' + c[0] + '|'); return false; }
        if (c[1] < 1 || c[1] > 12) { console.log('Der Beitrittsmonat ist ungültig. Aktueller Wert: |' + c[1] + '| vom Typ |' + c[1] + '|'); return false; }  
        if (c[2] < d1[2]) { console.log('Das Beitrittsjahr ist ungültig. Aktueller Wert: |' + c[2] + '| vom Typ |' + c[2] + '|'); return false; }

        return true;
    }

    isValiddateOfLeaving() {
        const dateFrom = "01/01/1900";
        var d1;
        var c;
        if(this.dateOfLeaving === undefined ) { console.log('Das Austrittsdatum ist ungültig. Aktueller Wert: |' + this.dateOfLeaving + '| vom Typ |' + typeof(this.dateOfLeaving) + '|'); return false; } 
        if(!(this.dateOfLeaving instanceof Date) && (this.dateOfLeaving !== null)) { console.log('Das Austrittsdatum ist ungültig. Aktueller Wert: |' + this.dateOfLeaving + '| vom Typ |' + typeof(this.dateOfLeaving) + '|'); return false; } 
        d1 = dateFrom.split("/"); 
        if(this.dateOfLeaving instanceof Date) {
            c = [this.dateOfLeaving.getDate(),this.dateOfLeaving.getMonth()+1,this.dateOfLeaving.getFullYear()]
            if (c[0] < 1 || c[0] > 31) { console.log('Der Austrittstag ist ungültig. Aktueller Wert: |' + c[0] + '| vom Typ |' + c[0] + '|'); return false; }
            if (c[1] < 1 || c[1] > 12) { console.log('Der Austrittssmonat ist ungültig. Aktueller Wert: |' + c[1] + '| vom Typ |' + c[1] + '|'); return false; }  
            if (c[2] < d1[2]) { console.log('Das Austrittsjahr ist ungültig. Aktueller Wert: |' + c[2] + '| vom Typ |' + c[2] + '|'); return false; }
        }
        return true;
    }

    isValidStreetName() {
        if(this.streetName === null || this.streetName === undefined || this.streetName === 0 || typeof(this.streetName) !== 'string') {
            console.log('Der Straßenname ist ungültig. Aktueller Wert: |' + this.streetName + '| vom Typ |' + typeof(this.streetName) + '|');
            return false;
        }
        return true;
    }

    isValidStreetNumber() {
        if(this.streetNumber === null || this.streetNumber === undefined || typeof(this.streetNumber) !== 'string') {
            console.log('Die Straßennummer ist ungültig. Aktueller Wert: |' + this.streetNumber + '| vom Typ |' + typeof(this.streetNumber) + '|');
            return false;
        }
        return true;
    }

    isValidPostalCode() {
        if(this.postalCode === null || this.postalCode === undefined || this.postalCode === 0 || typeof(this.postalCode) !== 'string') {
            console.log('Die Postleitzahl ist ungültig. Aktueller Wert: |' + this.postalCode + '| vom Typ |' + typeof(this.postalCode) + '|');
            return false;
        }
        return true;
    }

    isValidResidence() {
        if(this.residence === null || this.residence === undefined || this.residence === 0 || typeof(this.residence) !== 'string') {
            console.log('Der Wohnort ist ungültig. Aktueller Wert: |' + this.residence + '| vom Typ |' + typeof(this.residence) + '|');
            return false; 
        }
        return true;
    }

    isValidNationality() {
        if(this.nationality === null || this.nationality === undefined || this.nationality === 0 || typeof(this.nationality) !== 'string') {
            console.log('Die Nationalität ist ungültig. Aktueller Wert: |' + this.nationality + '| vom Typ |' + typeof(this.nationality) + '|');
            return false;
        }
        return true;
    }

    isValidJob() {
        if(this.job === null || this.job === undefined || this.job === 0 || typeof(this.job) !== 'string') {
            console.log('Der Jobstatus ist ungültig. Aktueller Wert: |' + this.job + '| vom Typ |' + typeof(this.job) + '|');
            return false;
        }
        return true;
    }

    isValidContactInfo() {
        if((this.mobileNumber === null || this.mobileNumber === undefined || this.mobileNumber === 0) && (this.landlineNumber === null || this.landlineNumber === undefined || this.landlineNumber === 0) && (this.email === null || this.email === undefined || this.email === 0)) { 
            console.log('Ungültige Kontaktinformationen! Die Mobilnummer, Festnetznummer und E-Mail wurden nicht angegeben.');
            return false;
        }
        return true;
    }

    isValidExecutivestatus() {
        if(this.isExecutive === null || this.isExecutive === undefined) {
            console.log('Der Exekutivstatus ist ungültig. Aktueller Wert: |' + this.isExecutive + '| vom Typ |' + typeof(this.isExecutive) + '|');
            return false;
        }
        if(typeof(this.isExecutive) !== 'boolean') {
            console.log('Der Exekutivstatus ist ungültig. Aktueller Wert: |' + this.isExecutive + '| vom Typ |' + typeof(this.isExecutive) + '|');
            return false;
        }
        return true;
    }

    isValidInactiveStatus() {
        if(this.isInactive === null || this.isInactive === undefined) {
            console.log('Der Ruhend-Status ist ungültig. Aktueller Wert: |' + this.isInactive + '| vom Typ |' + typeof(this.isInactive) + '|');
            return false;
        }
        if(typeof(this.isInactive) !== 'boolean') {
            console.log('Der Ruhend-Status ist ungültig. Aktueller Wert: |' + this.isInactive + '| vom Typ |' + typeof(this.isInactive) + '|');
            return false;
        }
        return true;
    }

    isValidTrainerstatus() {
        if(this.isTrainer === null || this.isTrainer === undefined) {
            console.log('Der Trainerstatus ist ungültig. Aktueller Wert: |' + this.isTrainer + '| vom Typ |' + typeof(this.isTrainer) + '|');
            return false;
        }
        if(typeof(this.isTrainer) !== 'boolean') {
            console.log('Der Trainerstatus ist ungültig. Aktueller Wert: |' + this.isTrainer + '| vom Typ |' + typeof(this.isTrainer) + '|');
            return false;
        }
        return true;
    }

    isValidRank() {
        if(this.rank === null || this.rank === undefined || this.rank === 0 || typeof(this.rank) !== 'string') {
            console.log('Das Geburtsjahr ist ungültig. Aktueller Wert: |' + this.rank + '| vom Typ |' + typeof(this.rank) + '|');
            return false;
        }
        return true;
    }

    isValidMember() {
        if (!this.isValidLastname()){ return false; }
        if (!this.isValidFirstname()){ return false; }
        if (!this.isValidGender()){ return false; }
        if (!this.isValiddateOfBirth()){ return false; } 
        if (!this.isValiddateOfAccession()){ return false; }
        if (!this.isValiddateOfLeaving()){ return false; }
        if (!this.isValidStreetName()){ return false; }
        if (!this.isValidStreetNumber()){ return false; }
        if (!this.isValidPostalCode()){ return false; }
        if (!this.isValidResidence()){ return false; }
        if (!this.isValidNationality()){ return false; }
        if (!this.isValidJob()){ return false; }
        if (!this.isValidContactInfo()){ return false; }
        if (!this.isValidExecutivestatus()){ return false; }
        if (!this.isValidInactiveStatus()){ return false; }
        if (!this.isValidTrainerstatus()){ return false; }
        if (!this.isValidRank()){ return false; }
        return true;
    }

    changeValues(lastname, firstname, gender, dateOfBirth, dateOfAccession, dateOfLeaving, streetName, streetNumber, postalCode, residence, nationality, job, mobileNumber, landlineNumber, email, isExecutive, isInactive, isTrainer, rank, comment) {
        if (this.lastname !== lastname) {
            this.lastname = lastname
        }
        
        // TODO check what needs more performance: if then change or everthing change
        this.firstname= firstname,
        this.gender= gender.toString().toLowerCase(),
        this.dateOfBirth= dateOfBirth,
        this.dateOfAccession= dateOfAccession,
        this.dateOfLeaving = dateOfLeaving,
        this.streetName= streetName,
        this.streetNumber= streetNumber,
        this.postalCode= postalCode,
        this.residence= residence,
        this.nationality= nationality,
        this.job= job,
        this.mobileNumber= mobileNumber,
        this.landlineNumber= landlineNumber,
        this.email= email,
        this.isExecutive= isExecutive,
        this.isInactive= isInactive,
        this.isTrainer= isTrainer,
        this.rank= rank,
        this.comment= comment
    }
}

window.addEventListener('load', function(){
    console.log('Member-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)