var view = (function () { // encapsulation / Kapselung

    let memberList;
    let filterOrder = 'asc';
    let isFullscreen = false;
    const tableHeadIds = ['memberListLastname', 'memberListFirstname', 'memberListGender', 'memberListDateOfBirth', 'memberListDateOfAccession',
                        'memberListDateOfLeaving', 'memberListStreetName', 'memberListStreetNumber', 'memberListPostalCode', 'memberListResidence',
                        'memberListNationality', 'memberListJob', 'memberListMobileNumber', 'memberListLandlineNumber', 'memberListEmail',
                        'memberListIsExecutive', 'memberListIsInactive', 'memberListIsTrainer', 'memberListRank', 'memberListComment'
                        ];

    initView = function() {
        changeOptionMenuVisability();
        controler.requestUpdateMemberList(); // async function
        

        document.getElementById('overlay').addEventListener('click', function() {changeOptionMenuVisability()});

        document.getElementById('overviewMenuEntry').addEventListener('click', function() {changeActiveView('overviewView', 'overviewMenuEntry')});
        document.getElementById('structureMenuEntry').addEventListener('click', function() {changeActiveView('structureView', 'structureMenuEntry')});
        document.getElementById('paidContrebutionMenuEntry').addEventListener('click', function() {changeActiveView('paidContrebutionView', 'paidContrebutionMenuEntry')});

        document.getElementById('buttonToogleFullscreen').addEventListener('click', function() {toggleFullscreen()});

        document.getElementById(tableHeadIds[0]).addEventListener('click', function() {sortMemberList(tableHeadIds[0])});
        document.getElementById(tableHeadIds[1]).addEventListener('click', function() {sortMemberList(tableHeadIds[1])});
        document.getElementById(tableHeadIds[2]).addEventListener('click', function() {sortMemberList(tableHeadIds[2])});
        document.getElementById(tableHeadIds[3]).addEventListener('click', function() {sortMemberList(tableHeadIds[3])});
        document.getElementById(tableHeadIds[4]).addEventListener('click', function() {sortMemberList(tableHeadIds[4])});
        document.getElementById(tableHeadIds[5]).addEventListener('click', function() {sortMemberList(tableHeadIds[5])});
        document.getElementById(tableHeadIds[6]).addEventListener('click', function() {sortMemberList(tableHeadIds[6])});
        document.getElementById(tableHeadIds[7]).addEventListener('click', function() {sortMemberList(tableHeadIds[7])});
        document.getElementById(tableHeadIds[8]).addEventListener('click', function() {sortMemberList(tableHeadIds[8])});
        document.getElementById(tableHeadIds[9]).addEventListener('click', function() {sortMemberList(tableHeadIds[9])});
        document.getElementById(tableHeadIds[10]).addEventListener('click', function() {sortMemberList(tableHeadIds[10])});
        document.getElementById(tableHeadIds[11]).addEventListener('click', function() {sortMemberList(tableHeadIds[11])});
        document.getElementById(tableHeadIds[12]).addEventListener('click', function() {sortMemberList(tableHeadIds[12])});
        document.getElementById(tableHeadIds[13]).addEventListener('click', function() {sortMemberList(tableHeadIds[13])});
        document.getElementById(tableHeadIds[14]).addEventListener('click', function() {sortMemberList(tableHeadIds[14])});
        document.getElementById(tableHeadIds[15]).addEventListener('click', function() {sortMemberList(tableHeadIds[15])});
        document.getElementById(tableHeadIds[16]).addEventListener('click', function() {sortMemberList(tableHeadIds[16])});
        document.getElementById(tableHeadIds[17]).addEventListener('click', function() {sortMemberList(tableHeadIds[17])});
        document.getElementById(tableHeadIds[18]).addEventListener('click', function() {sortMemberList(tableHeadIds[18])});
        document.getElementById(tableHeadIds[19]).addEventListener('click', function() {sortMemberList(tableHeadIds[19])});
    },

    changeOptionMenuVisability = function() {
        document.getElementById('overlay').classList.toggle("hidden")
    },

    updateMemberList = function(memberList) {
        let trElement, i, tdElement;

        const tbodyElement = document.getElementById('currentMemberList').getElementsByTagName('tbody')[0];
        const numberOfAttributes = tableHeadIds.length;
        
        memberList.forEach(function(memberEntry, id) { // value (), key (String)
            trElement = document.createElement('TR');
            trElement.id = 'member' + id;
            
            // formating Dates  
            memberEntry.dateOfBirth = toEuropeanDateFormat(memberEntry.dateOfBirth);
            memberEntry.dateOfAccession = toEuropeanDateFormat(memberEntry.dateOfAccession);
            memberEntry.dateOfLeaving = toEuropeanDateFormat(memberEntry.dateOfLeaving);

            // formating potencial emtpy values
            if(memberEntry.dateOfLeaving == '') {
                memberEntry.dateOfLeaving = '-';
            }
            if(memberEntry.mobileNumber == null) {
                memberEntry.mobileNumber = '-';
            }
            if(memberEntry.landlineNumber == null) {
                memberEntry.landlineNumber = '-';
            }
            if(memberEntry.email == null) {
                memberEntry.email = '-';
            }
            if(memberEntry.comment == '') {
                memberEntry.comment = '-';
            }

            // write attributes
            for(i = 0; i < numberOfAttributes; i++) {
                tdElement = document.createElement('TD');
                tdElement.innerText = Object.values(memberEntry)[i];    
                trElement.appendChild(tdElement);
            }

            tbodyElement.appendChild(trElement);
        });
        
    },

    toEuropeanDateFormat = function(currentDate) {
        let day, month, year, formatedDate;

        if (currentDate === null) {
            formatedDate = '';
        } else {
            day = fillDigitForDate( (currentDate.getDate()) )
            month = fillDigitForDate( (currentDate.getMonth() + 1) );
            year = currentDate.getFullYear();
    
            formatedDate = day + '.' + month + '.' + year;
        }

        return formatedDate
    },

    fillDigitForDate = function(datePart) {
        let result

        if(datePart < 10) {
            result = '0' + datePart;
        } else {
            result = datePart;
        }

        return result;
    }, 

    changeActiveView = function(newActiveView, newActiveMenuEntry) {
        const activeViewClassName = 'activeView'
        const activeNavEntryClassName = 'activeNavEntry'
        const hiddenClassName = 'hidden'
        var currentActiveNavEntry, bevoreActiveViewClassList, newActiveViewClassList, bevoreActiveNavEntryClassName, newActiveNavEntryClassName
        
        currentActiveNavEntry = document.getElementsByClassName(activeNavEntryClassName)[0]

        if(currentActiveNavEntry.id !== newActiveMenuEntry) {
            bevoreActiveViewClassList = document.getElementsByClassName(activeViewClassName)[0].classList;
            bevoreActiveViewClassList.toggle(activeViewClassName)
            bevoreActiveViewClassList.toggle(hiddenClassName)

            newActiveViewClassList = document.getElementById(newActiveView).classList;
            newActiveViewClassList.toggle(activeViewClassName)
            newActiveViewClassList.toggle(hiddenClassName)

            bevoreActiveNavEntryClassName = currentActiveNavEntry.classList;
            bevoreActiveNavEntryClassName.toggle(activeNavEntryClassName)

            newActiveNavEntryClassName = document.getElementById(newActiveMenuEntry).classList;
            newActiveNavEntryClassName.toggle(activeNavEntryClassName)
        }

        document.getElementById('toggleDropdownView').checked = false;
    },

    sortMemberList = function(thId) {
        let unsortedIndex, compareIndexLimit, currentCompareIndex, currentRowElement, currentAttribute, nextElement, nextAttribute;

        if(this.filterOrder === 'asc') {
            this.filterOrder = 'desc';
        } else {
            this.filterOrder = 'asc'
        }
        const currentFilterOrder = this.filterOrder;

        const idIndex = filterIdIndex(thId);

        const tbodyElement = document.getElementById('currentMemberList').getElementsByTagName('tbody')[0]; // parent
        const countOfMember = tbodyElement.childElementCount;
        
        for(unsortedIndex = countOfMember; unsortedIndex > 1; unsortedIndex--) { // this is bubblesort
            compareIndexLimit = unsortedIndex - 1;
            for (currentCompareIndex=0; currentCompareIndex < compareIndexLimit; currentCompareIndex++) {
            
                currentRowElement = tbodyElement.getElementsByTagName('tr')[currentCompareIndex];
                currentAttribute = currentRowElement.getElementsByTagName('td')[idIndex].innerText;
                nextElement =  tbodyElement.getElementsByTagName('tr')[currentCompareIndex+1];
                nextAttribute = nextElement.getElementsByTagName('td')[idIndex].innerText;
    
                if(currentFilterOrder === 'asc') {
                    if(currentAttribute >= nextAttribute) {
                        tbodyElement.insertBefore(nextElement, currentRowElement)
                    }
                } else { // desc
                    if(currentAttribute <= nextAttribute) {
                        tbodyElement.insertBefore(nextElement, currentRowElement)
                    }
                }
                
            }
        }

        console.log('Sortiere nach |' + thId + '| in der Reihenfolge |' + currentFilterOrder + '|')
    },

    filterIdIndex = function(targetId) {
        let currentAttributeIndex;
        const lastIndexForAttributes = (tableHeadIds.length - 1);
        const thCollection = document.getElementById('currentMemberList').getElementsByTagName('thead')[0].getElementsByTagName('tr')[0].getElementsByTagName('th');
        for(currentAttributeIndex=0; currentAttributeIndex <= lastIndexForAttributes; currentAttributeIndex++) {
            if(thCollection[currentAttributeIndex].id === targetId) {
                return currentAttributeIndex;
            }
        }
    },

    /**
     * eng: report function. Execute everything needed to (de)activate the fullscreen mode.
     * ger: Melde-Funktion. Fuehrt alles noetige aus, um den Vollbild-Modus zu (de)aktivieren.
     */
    toggleFullscreen = function () {
        var elem = document.documentElement;

        if (isFullscreen) {
            if (document.exitFullscreen) {
                document.exitFullscreen();
              } else if (document.mozCancelFullScreen) { /* Firefox */
                document.mozCancelFullScreen();
              } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                document.webkitExitFullscreen();
              } else if (document.msExitFullscreen) { /* IE/Edge */
                document.msExitFullscreen();
              }
            isFullscreen = false;    
        } else {
            if (elem.requestFullscreen) {
            elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
            elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
            }

            isFullscreen = true;
        }

        document.getElementById('toggleDropdownOption').checked = false; // close Menu
    };

    // public API / oeffentliche API
    return { 
        reportUpdatedMemberlist: function(memberList) {
            updateMemberList(memberList)
        }
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function(){
    initView();
    console.log('View-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)
