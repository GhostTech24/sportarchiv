var controler = (function () { // encapsulation / Kapselung

    // public API / oeffentliche API
    return { 
        requestUpdateMemberList: async function() {          
            await model.loadMemberList();
            const updatedMemberList = model.getMemberList()
            
            this.reportUpdatedMemberList(updatedMemberList)
        },

        reportUpdatedMemberList: function(updatedMemberList) {
            view.reportUpdatedMemberlist(updatedMemberList);
        }
    };
    
})(); // end of encapsulation / Ende der Kapselung

window.addEventListener('load', function(){
    console.log('Controler-Modul ist bereit. |' + Date(Date.now()).slice(4,24));
}, false)