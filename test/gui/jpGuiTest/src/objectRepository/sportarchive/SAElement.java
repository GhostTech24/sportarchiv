package objectRepository.sportarchive;

import org.openqa.selenium.WebElement;
import objectRepository.GenericRepository;

public final class SAElement extends GenericRepository {
	
	public final static WebElement getAppNameElement() {
		final String id = "appName";
		
		return getElement(id);
	}
	
	// list
	public final static WebElement getDropdownView() {
		final String id = "dropdownView";
		
		return getElement(id);
	}
	
	public final static WebElement getOverviewMenuEntry() { // view 1
		final String id = "overviewMenuEntry";
		
		return getElement(id);
	}
	
	public final static WebElement getStructureMenuEntry() { // view 2
		final String id = "structureMenuEntry";
		
		return getElement(id);
	}
	
	public final static WebElement getPaidContrebutionMenuEntry() { // view 3
		final String id = "paidContrebutionMenuEntry";
		
		return getElement(id);
	}
	
	public final static WebElement getButtonDropdownOptionElement() {
		final String id = "buttonDropdownOption";
		
		return getElement(id);
	}
	
	
	public final static WebElement getCurrentMemberList() {
		final String id = "currentMemberList";
		
		return getElement(id);
	}
}
