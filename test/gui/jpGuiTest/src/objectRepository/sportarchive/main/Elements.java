package objectRepository.sportarchive.main;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import objectRepository.GenericRepository;

public class Elements extends GenericRepository {
	
	
	public final static WebElement getOverviewView() { // view 1
		final String id = "overviewView";
		
		return getElement(id);
	}
	
	public final static WebElement getStructureView() { // view 2
		final String id = "structureView";
		
		return getElement(id);
	}
	
	public final static WebElement getPaidContrebutionView() { // view 3
		final String id = "paidContrebutionView";
		
		return getElement(id);
	}
	
	public final static WebElement getMemberTrack(final int trackNumber) {
		final String tableId = "currentMemberList";
		final int searchIndex = trackNumber - 1;
		final WebElement tableElement, tBodyElement, trackElement;
		final List<WebElement> foundedElements;
		
		tableElement = getElement(tableId);
		tBodyElement = tableElement.findElement(By.tagName("tbody"));
		foundedElements = tBodyElement.findElements(By.tagName("tr"));
		
		if(foundedElements.size() != searchIndex) {
			trackElement = foundedElements.get(searchIndex);
			
			return trackElement;
		} else {
			return null;
		}
	}
	
}
