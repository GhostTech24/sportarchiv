package objectRepository.sportarchive.header;

import org.openqa.selenium.WebElement;

import objectRepository.GenericRepository;

public class GenericElements extends GenericRepository {
	public final static WebElement getAppNameElement() {
		final String id = "appName";
		
		return getElement(id);
	}
	
	// Dropdownmenu
	public final static WebElement getButtonDropdownOption() {
		final String id = "buttonDropdownOption";
		
		return getElement(id);
	}
	
	public final static WebElement getButtonToogleFullscreen() {
		final String id = "buttonToogleFullscreen";
		
		return getElement(id);
	}
}
