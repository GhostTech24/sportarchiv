package objectRepository.wikipedia;

import org.openqa.selenium.WebElement;

import objectRepository.GenericRepository;



public class WikiRep extends GenericRepository{	
	
	public static final WebElement inputSearchbar() {
		final String id = "txtSearch";
		
		return getElement(id);
	}
}
