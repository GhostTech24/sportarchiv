package test.sportarchive.main.overviewView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import objectRepository.sportarchive.SAElement;
import objectRepository.sportarchive.main.Elements;
import testEngine.TestController;
import testEngine.WebAction;

public class CheckMemberlistVisible {
	
	private final static List<String> member1 = new ArrayList<String>(Arrays.asList("Demoman",		"First",	"m", "01.01.2000", "01.01.2018", "-", "Beispielstraße", "1",	"56626", "Andernach",	"GER", "Softwaretester",	"-", "-", 			"firstDemoman@hotmail.com",	"false", "false", "false", "Fortgeschrittener", "Diabetiker"));
	private final static List<String> member2 = new ArrayList<String>(Arrays.asList("Demowoman",	"Second",	"w", "02.02.2000", "02.02.2018", "-", "Beispielstr.",	"2b",	"56575", "Weißenthurm",	"GER", "Verkäuferin",		"-", "02637123456", "-",						"false", "false", "false", "Fortgeschrittener", "-"));
	// 
	public final static void execute() {
		System.out.println(String.format("Führe den Test |%s| aus.", CheckMemberlistVisible.class.getSimpleName()));
		
		// Given 		
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		WebAction.deleteIndexedDb("MyDb");
				
		WebAction.click(SAElement.getOverviewMenuEntry());
		WebAction.isVisible(Elements.getOverviewView(), true);
		WebAction.click(Elements.getOverviewView());
		
		// When and Then
		WebAction.validateTableTrack(Elements.getMemberTrack(1), member1, true);
		WebAction.validateTableTrack(Elements.getMemberTrack(2), member2, true);
		
		// reset
		// -
	}
}
