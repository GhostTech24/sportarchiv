package test.sportarchive.header;

import objectRepository.sportarchive.header.GenericElements;
import testEngine.TestController;
import testEngine.WebAction;

public class ToggleFullscreen {
	
	
	
	public final static void execute() {
		System.out.println(String.format("F�hre den Test |%s| aus.", ToggleFullscreen.class.getSimpleName()));
		
		// Given 
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		if(WebAction.isFullscreen(false)) {
			return;
		}
		
		// When
		WebAction.click(GenericElements.getButtonDropdownOption());
		WebAction.click(GenericElements.getButtonToogleFullscreen());
		
		//Then
		WebAction.isFullscreen(true);
		
		// reset
		WebAction.click(GenericElements.getButtonDropdownOption());
		WebAction.click(GenericElements.getButtonToogleFullscreen());
		WebAction.isFullscreen(false);
	}
}
