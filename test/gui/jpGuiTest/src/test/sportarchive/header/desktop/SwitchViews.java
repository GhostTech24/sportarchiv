package test.sportarchive.header.desktop;

import objectRepository.sportarchive.SAElement;
import objectRepository.sportarchive.main.Elements;
import testEngine.TestController;
import testEngine.WebAction;

public class SwitchViews {
	
	public final static void execute() {
		System.out.println(String.format("F�hre den Test |%s| aus.", SwitchViews.class.getSimpleName()));
		
		// Given
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		WebAction.isVisible(SAElement.getDropdownView(), true);
		WebAction.isVisible(SAElement.getOverviewMenuEntry(), true);
		WebAction.isVisible(SAElement.getStructureMenuEntry(), true);
		WebAction.isVisible(SAElement.getPaidContrebutionMenuEntry(), true);
		
		WebAction.click(SAElement.getOverviewMenuEntry());
		WebAction.isVisible(Elements.getOverviewView(), true);
		WebAction.isVisible(Elements.getStructureView(), false);
		WebAction.isVisible(Elements.getPaidContrebutionView(), false);
		
		WebAction.click(SAElement.getStructureMenuEntry());
		WebAction.isVisible(Elements.getOverviewView(), false);
		WebAction.isVisible(Elements.getStructureView(), true);
		WebAction.isVisible(Elements.getPaidContrebutionView(), false);
		
		WebAction.click(SAElement.getPaidContrebutionMenuEntry());
		WebAction.isVisible(Elements.getOverviewView(), false);
		WebAction.isVisible(Elements.getStructureView(), false);
		WebAction.isVisible(Elements.getPaidContrebutionView(), true);
		
		//reset
		WebAction.click(SAElement.getOverviewMenuEntry());
	}
	
}
