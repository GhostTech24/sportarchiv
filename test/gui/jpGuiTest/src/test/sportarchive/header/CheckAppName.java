package test.sportarchive.header;

import objectRepository.sportarchive.header.GenericElements;
import testEngine.TestController;
import testEngine.WebAction;

public class CheckAppName {
	
	private final static String expectedText = "Sportarchive";
	
	public final static void execute() {
		System.out.println(String.format("F�hre den Test |%s| aus.", CheckAppName.class.getSimpleName()));
		
		// Given 
		// -
		
		// When
		WebAction.navigateToUrl(TestController.getAppUrl());
		WebAction.waitForPageLoad();
		
		//Then
		WebAction.isText(GenericElements.getAppNameElement(), expectedText);
		
		// reset
		// -
	}
}
