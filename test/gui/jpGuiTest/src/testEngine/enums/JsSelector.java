package testEngine.enums;

public enum JsSelector {
	ID,
	CLASS,
	NAME,
	TAG;
}
