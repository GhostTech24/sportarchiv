package testEngine;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import objectRepository.GenericRepository;
import testEngine.enums.JsSelector;

/**
 * Enthaelt allgemeine Test-Methoden.
 */
public final class WebAction {
	private static WebDriver currentDriver;
	
	public final static void click(final WebElement element) {
		if(element.isDisplayed()) {
			element.click();
			System.out.println(String.format("Es wurde auf folgendes Element geklickt |%s|", element.toString()));
		} else {
			System.err.println(String.format("Auf das Element |%s| wurde nicht geklickt, da es nicht sichtbar ist!", element.toString()));
		}
	}
		
	public final static void deleteIndexedDb(String... dbNames) {
		final String javascript = "indexedDB.deleteDatabase(arguments[0])";
		final int lastIndex = dbNames.length - 1;
		final long toleratedWaitingTime = 3000;
		
		String formatedDbNames = "";
		
		for(String currentDb: dbNames) {
			executeJavascript(javascript, currentDb);
			
			if(!dbNames[lastIndex].equals(currentDb)) {
				formatedDbNames = formatedDbNames + ", ";
			}		
			formatedDbNames = formatedDbNames + currentDb;
		}
		
		currentDriver.navigate().refresh();
		isPageLoaded(toleratedWaitingTime);
		System.out.println(String.format("Folgende IndexDBs wurden gelöscht (und mit einem Refresh versetzte): |%s|", formatedDbNames));
		
	}
	
	public final static void isChecked(final WebElement element, final boolean shouldChecked) {
		final boolean isChecked;
		
		isChecked = element.isSelected();
		
		if(shouldChecked == isChecked) {
			System.out.println(String.format("Der Check-Status des Elements |%s| entspricht der Erwartung. Check-Status ist: |%s|", element, shouldChecked));
		} else {
			System.err.println(String.format("Der Check-Status des Elements |%s| entspricht nicht der Erwartung! Check-Status soll: |%s|, aber war |%s|", element, shouldChecked, isChecked));
		}
	}
	
	public final static void isVisible(final WebElement element, final boolean shouldVisible) {
		final boolean isVisible;
		
		isVisible = element.isDisplayed();
		
		if(shouldVisible == isVisible) {
			System.out.println(String.format("Die Sichbarkeit des Elements |%s| entspricht der Erwartung. Sichtbarkeit ist: |%s|", element, shouldVisible));
		} else {
			System.err.println(String.format("Die Sichbarkeit des Elements |%s| entspricht nicht der Erwartung! Sichtbarkeit soll: |%s|, aber war |%s|", element, shouldVisible, isVisible));
		}
	}
	
	public final static void isText(final WebElement element, final String expectedText) {
		final String currentText;
		
		currentText = element.getText();
		
		if(expectedText.equals(currentText)) {
			System.out.println(String.format("Der Text des Elements |%s| entspricht der Erwartung. Text ist: |%s|", element, expectedText));
		} else {
			System.err.println(String.format("Der Text des Elements |%s| entspricht nicht der Erwartung! Text soll: |%s|, aber war |%s|", element, expectedText, currentText));
		}
	}
	
	public final static boolean isFullscreen(final boolean shouldBeFullscreen) {
		final boolean isFullscreen;
		final String javascript = "return document.fullscreen";
		
		isFullscreen = (boolean) executeJavascript(javascript);
		
		if(shouldBeFullscreen == isFullscreen) {
			System.out.println(String.format("Der Vollbild-Modus entspricht der Erwartung. Vollbild ist: |%s|", shouldBeFullscreen));
		} else {
			System.err.println(String.format("Der Vollbild-Modus entspricht nicht der Erwartung! Sichtbarkeit soll: |%s|, aber war |%s|", shouldBeFullscreen, isFullscreen));
		}
		
		return isFullscreen;
	}
	
	public final static void navigateToUrl(final String url) {
		
		System.out.println(String.format("Es wird auf folgende URL navigiert: |%s|", url));
		
		currentDriver.navigate().to(url); // working example: "https://www.google.de"
//		driver.get(url); // get = navigateTO and WaitForPageload
	}	
	
	public final static void setDriver(final WebDriver driver) {
		currentDriver = driver;
	}
	
	/**
	 * Warte-Funktion. Warte eine bestimmte Zeit, bevor Programm fortgesetzt wird. <br>
	 * Primaer nur fuer Debugging-Zwecke geeignet! Es ist immer besser, konditionierte Warte-Methoden zu verwenden.
	 * @param milliseconds
	 */
	public final static void wait(final int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(String.format("Der Testablauf wartete |%s| Millisekunden.", milliseconds));
	}
	
	/**
	 * TODO checken obs wirklich funktioniert.
	 * @param elementAsStartPoint
	 * @param pixels
	 */
	public final static void swipeLeft(final WebElement elementAsStartPoint, final int pixels) {
		Actions action = new Actions(currentDriver);
		action.dragAndDropBy(elementAsStartPoint, 200, 0).build().perform();
	}
	
	@SuppressWarnings("unchecked")
	public final static void scroll(final JsSelector findBy, final String searchValue, final int xAxisPixels, final int yAxisPixels) {
		final String byId = "getElementById";
		final String byClass = "getElementsByClassName";
		final String byName = "getElementsByName";
		final String byTag = "getElementsByTagName";
		
		final long isUniqueValue = 1;
		
		final String currentSearchMethod;
		final String searchJavascriptWithArgs = "return document.%s('%s').length"; // can't put String as function-name -> TypeError
		final String searchJavascriptScrollElementsWithArgs = "return document.%s('%s')"; // can't put String as function-name -> TypeError
		final Object searchResult;
		final List<WebElement> searchResultList;
		final long resultValue;
		final WebElement scrollElement;
		
		
		
		if(xAxisPixels == 0 && yAxisPixels == 0) {
			System.err.println(String.format("Es wurde nicht gescrollt, da keine der Werte eine Veränderung bewirken kann war. Wert: (X: |%s|, Y: |%s|", xAxisPixels, yAxisPixels));
			return;
		}
		
		switch(findBy) {
			case ID:
				currentSearchMethod = byId;
			break;
			
			case CLASS:
				currentSearchMethod = byClass;
			break;
			
			case NAME:
				currentSearchMethod = byName;
			break;
			
			case TAG:
				currentSearchMethod = byTag;
			break;
			
			default:
				System.err.println(String.format("Es wurde nicht gescrollt, da ein ungültiger Suchselektor verwendet wurde. Verwendet wurde: |%s|", findBy));	
				return;
		}
		
		searchResult = executeJavascript(String.format(searchJavascriptWithArgs, currentSearchMethod, searchValue));
		
		if(findBy.equals(JsSelector.ID)) { // null or webelement
			
			if(searchResult == null) {
				System.err.println(String.format("Es wurde nicht gescrollt, da mit den Suchkriterien ein falsches Objekt gefunden wurde. Gesuchte wurde mit |%s| , aber es war 'null'.", searchJavascriptWithArgs));
				return;
			}
			
			scrollElement = (WebElement) searchResult;
			
		} else { // List<WebElement> from tag, class or name
			if(!(searchResult instanceof Long)) {
				System.err.println(String.format("Es wurde nicht gescrollt, da mit den Suchkriterien ein falsches Objekt gefunden wurde. Gesuchte wurde mit |%s| nach einem 'Long', aber es war |%s|.", searchJavascriptWithArgs, searchResult.getClass().toString()));
				return;
			}
			
			resultValue = (long) searchResult;
			
			if(isUniqueValue != resultValue) {
				System.err.println(String.format("Es wurde nicht gescrollt, da mit den Suchkriterien kein eindeutiges Ergebnis gefunden wurde. Gesuchte wurde via |%s|, gefunden wurden |%s| Ergebnise.", searchJavascriptWithArgs, resultValue));	
				return;
			}
			
			searchResultList = (List<WebElement>) executeJavascript(String.format(searchJavascriptScrollElementsWithArgs, currentSearchMethod, searchValue));
			scrollElement = searchResultList.get(0);			
		}

		if(!isScrollableElement(scrollElement)) {
			System.err.println(String.format("Es wurde nicht gescrollt, da das Element |%s| nicht gescrollt werden kann!", scrollElement));
			return;
		}
		
		scroll(scrollElement, xAxisPixels, yAxisPixels);
		
		System.out.println(String.format("Das Element |%s| wurde gescrollt um: (X:%s) (Y:%s)", scrollElement, xAxisPixels, yAxisPixels));
	}
	
	public final static void validateTableTrack(final WebElement trElement, final List<String> expectedValues, final boolean shouldScrollable) {
		final String errorMessageWrongInputElementArgs = "Das Element der Tabellen-Reihe entspricht nicht der Erwartung! Erwartet wurde ein TR-Element, aber es war |%s|";
		final String errorMessageWrongTagWithArgs = "Der Tag der Tabellen-Reihe entspricht nicht der Erwartung! Erwartet wurde |%s|, aber es war |%s|";
		final String errorMessageWrongSizeWithArgs = "Die Spaltenanzahl der Tabellen-Reihe entspricht nicht der Erwartung! Erwartet wurde |%s|, aber es war |%s|";
		final String errorMessageWrongScrollableWithArgs = "Die Scroll-Fähigkeit der Tabellen-Reihe entspricht nicht der Erwartung! Erwartet wurde |%s|, aber es war |%s|";
		final String errorMessageWrongValueWithArgs = "Der Wert von Spalte |%s| der Tabellen-Reihe entspricht nicht der Erwartung! Erwartet wurde |%s|, aber es war |%s|";
		final String errorMessageWrongVisibilityNoScrollWithArgs = "Die Sichtbarkeit von Spalte |%s| mit dem Wert |%s| der Tabellen-Reihe entspricht nicht der Erwartung! Der Container erlaubte kein Scrollen.";
		final String errorMessageWrongVisibilityWithScrollWithArgs = "Die Sichtbarkeit von Spalte |%s| mit dem Wert |%s| der Tabellen-Reihe entspricht nicht der Erwartung! Der Container erlaubte Scrollen.";	
		final String confirmMessage = "Die Tabellen-Reihe entspricht der Erwartung. Erwartet wurden die Werte: |%s|";
		
		final String expectedTag ="tr";
		final String elementWithoutParentTag = "html";
		final String javascriptScrollMaxWithArgs = "return (arguments[0].scrollWidth - arguments[0].clientWidth)";
			
		final String javascriptCurrentScrollWithArgs = "return arguments[0].scrollLeft";
		final String currentTag;
		final int expectedTdSize, currentTdSize;
		final List<WebElement> tdElements;
		
		WebElement scrollCheckElement;

		int scrollLeftMaxValue = 0;
		boolean isScrollable = false;
		String scrollCheckElementTag;
		WebElement currentTdElement;
		String currentTdValue;
		String expectedTdValue;
		boolean currentTdIsVisible;
		int currentScrollLeftValue;
		
		if(trElement == null) {
			System.err.println(String.format(errorMessageWrongInputElementArgs, trElement));
			return;
		}
		
		currentTag = trElement.getTagName();
		if(!expectedTag.equals(currentTag)) {
			System.err.println(String.format(errorMessageWrongTagWithArgs, expectedTag, currentTag));
			return;
		}
		
		expectedTdSize = expectedValues.size();	
		tdElements = trElement.findElements(By.tagName("td"));
		currentTdSize = tdElements.size();	
		if(expectedTdSize != currentTdSize) {
			System.err.println(String.format(errorMessageWrongSizeWithArgs, expectedTdSize, currentTdSize));
			return;
		}
		
		scrollCheckElement = trElement;
		do {
			scrollCheckElement = GenericRepository.getParentElement(scrollCheckElement);
			
			if(isScrollableElement(scrollCheckElement)) {
				isScrollable = true;
				scrollLeftMaxValue = (int)(long)executeJavascript(javascriptScrollMaxWithArgs, scrollCheckElement);
				scroll(scrollCheckElement, Integer.MIN_VALUE, 0); // scroll complete to left side
				break;
			}
			
			scrollCheckElementTag = scrollCheckElement.getTagName();
		}while(!scrollCheckElementTag.equals(elementWithoutParentTag));	
		
		if(shouldScrollable != isScrollable) {
			System.err.println(String.format(errorMessageWrongScrollableWithArgs, shouldScrollable, isScrollable));
			return;
		}
		
		for (int index = 0; index < currentTdSize; index++) {
			currentTdElement = tdElements.get(index);
			currentTdValue = currentTdElement.getText();
			expectedTdValue = expectedValues.get(index);
			
			if(!expectedTdValue.equals(currentTdValue)) {
				System.err.println(String.format(errorMessageWrongValueWithArgs, (index+1), expectedTdValue, currentTdValue));
				return;
			}
			
			do {
				
				currentTdIsVisible = isFullVisible(currentTdElement);
				
				if(currentTdIsVisible) {
					break;
				} else {
					if(shouldScrollable) {
						
						scroll(scrollCheckElement, 100, 0);
						currentScrollLeftValue = (int)(long)executeJavascript(javascriptCurrentScrollWithArgs, scrollCheckElement);
					} else {
						System.err.println(String.format(errorMessageWrongVisibilityNoScrollWithArgs, index, expectedTdValue));
						return;
					}
				}
				
			}while(scrollLeftMaxValue > currentScrollLeftValue);
			currentTdIsVisible = isFullVisible(currentTdElement);
			
			if(!currentTdIsVisible) {
				System.err.println(String.format(errorMessageWrongVisibilityWithScrollWithArgs, index, expectedTdValue));
				return;
			}
		}
		
		System.out.println(String.format(confirmMessage, expectedValues.toString()));
	}
	
	public final static void waitForPageLoad() {
		final long toleratedWaitingTime = 3000;
		
		if(!isPageLoaded(toleratedWaitingTime)) {
			System.err.println(String.format("Das Warten auf das Laden der Seite war fehlerhaft! Die tolerierte Wartezeit von |%s| Sekunden wurde überschritten!", toleratedWaitingTime));		
			return;
		}
		
		System.out.println("Das Warten auf das Laden der Seite war erfolgreich.");
	}
	
	public final static boolean isScrollableElement(final WebElement element) {
		final String javascriptCssOverflowValue = "return (getComputedStyle(arguments[0]).overflow)";
		final String cssScrollString = "scroll";
		final Object cssOverflowValueObject;
		final String cssOverflowValue;
		
		cssOverflowValueObject = executeJavascript(javascriptCssOverflowValue, element);
		cssOverflowValue = cssOverflowValueObject.toString();
		
		if(cssOverflowValue.contains(cssScrollString)) {
			return true;
		} else {
			return false;
		}
	}
	
	private final static Object executeJavascript(final String javascript, final Object... args) {
		final JavascriptExecutor jsE;
		final Object result;
		
		jsE = (JavascriptExecutor) currentDriver;  
		result = jsE.executeScript(javascript, args);
		
		return result;
	}
	
	/**
	 * Scroll-Funktion. Scrollt auf dem uebergebenen Element. <br><br>
	 * 
	 * @param element
	 * @param xAxis
	 * @param yAxis
	 */
	private final static void scroll(final WebElement element, final int xAxis, final int yAxis)   {
		final String scrollJavascript;
		
		scrollJavascript = "arguments[0]"  + String.format(".scrollBy(%s, %s)", xAxis, yAxis);
			
		executeJavascript(scrollJavascript, element);
	}
	
	/**
	 * eng: validation function. Validates the full visability on the viewport from an element. <br>
	 * ger: Validierungsfunktion. Validiert die vollstaendige Sichtbarkeit eines Elements im Viewport. <br><br>
	 * 
	 * See reference / siehe Referenz (2020.10.02):<br>  https://stackoverflow.com/questions/123999/how-can-i-tell-if-a-dom-element-is-visible-in-the-current-viewport/7557433#7557433
		
	 * @param element
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private final static boolean isFullVisible(final WebElement element) {
		final String javascriptElementRec = "return arguments[0].getBoundingClientRect()";
		final String javascriptInnerHeight = "return window.innerHeight";
		final String javascriptClientHight = "return document.documentElement.clientHeight";
		final String javascriptInnerWidth = "return window.innerWidth";
		final String javascriptClientWidth = "return document.documentElement.clientWidth";
		
		final Map boundingClientRec; 
		final double top, bottom, left, right;
		final double innerHeight, clientHight, innerWidth, clientWidth;
		
		boundingClientRec = (Map)executeJavascript(javascriptElementRec, element);
		innerHeight = (double)(long)executeJavascript(javascriptInnerHeight);
		clientHight = (double)(long)executeJavascript(javascriptClientHight);
		innerWidth = (double)(long)executeJavascript(javascriptInnerWidth);
		clientWidth = (double)(long)executeJavascript(javascriptClientWidth);
		
		top = Double.parseDouble(boundingClientRec.get("top").toString());
		bottom = Double.parseDouble(boundingClientRec.get("bottom").toString());
		left = Double.parseDouble(boundingClientRec.get("left").toString());
		right = Double.parseDouble(boundingClientRec.get("right").toString());
		
		if(top < 0) {
			return false;
		}
		if(left < 0) {
			return false;
		}
		
		if(bottom > innerHeight || bottom > clientHight) {
			return false;
		}
		
		if(right > innerWidth || bottom > clientWidth) {
			return false;
		}
		
		return true;
	}
	
	private final static boolean isPageLoaded(final long toleratedWaitingTime) {	
		final long askIntervall = 1000;
		long currentWaitedTime = 0;
		String returnValue;
		final String js = "return document.readyState;";
		
		
		while(currentWaitedTime<= toleratedWaitingTime) {		
			returnValue = executeJavascript(js).toString();		
			
			try { // wait first to give a open site time to change after key "return"
				Thread.sleep(askIntervall);
				currentWaitedTime =+ askIntervall;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if(returnValue.equals("complete")) {
				return true;
			}
			
		}
		
		return false;
	}
}
