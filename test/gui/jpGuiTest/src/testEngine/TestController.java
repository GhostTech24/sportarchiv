package testEngine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

//import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import objectRepository.sportarchive.SAElement;

public class TestController {
	
	private static String appUrl;
	private static String firefoxExePath;
	private static String firefoxDriverPath;
	private static String chromeExePath;
	private static String chromeDriverPath;
	
	private static final String configFilePath = "../appConfig.properties"; // from user.dir (java project) up to folder "gui"
	
	private static WebDriver driver;
	private static int hashCode;
	
	public final static WebDriver getDriver(final String browser) {
		if(driver == null) {
			fetchConfig();
			
			if(browser.equals("firefox")) {
				driver = initFirefoxDriver();
			}
			if(browser.equals("chrome")) {
				driver = initChromeDriver();
			}
		
			WebAction.setDriver(driver); // Engine
			SAElement.setWebDriver(driver); // O-Repositories; one time setting is enough; the abstract class contains it for every child!
		}
		
		return driver;
	}
	
	public final static String getAppUrl() {
		return appUrl;
	}
	
	public final static void setAppUrl(final String appUrl) {
		TestController.appUrl = appUrl;
	}
	
	public final static void closeDriver() {
		driver.close(); // .quit doesn't close Chrome!
		driver = null;
		
		System.out.println(String.format("Der Driver mit dem Hash |%s| wurde beendet.", hashCode));
	}
	
	private final static WebDriver initChromeDriver() {
		
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		ChromeOptions options = new ChromeOptions();
		options.setBinary(chromeExePath);
		options.addArguments("--remote-debugging-port=9225");	
		
		driver = new ChromeDriver(options);
		hashCode = driver.hashCode();
		
		System.out.println(String.format("Der Driver mit dem Hash |%s| wurde initialisiert.", hashCode));
		return driver;
	}
	
	private final static WebDriver initFirefoxDriver() {
		System.setProperty("webdriver.gecko.driver", firefoxDriverPath);
		System.setProperty("webdriver.firefox.bin", firefoxExePath);
		
		driver = new FirefoxDriver();
		
//		driver.manage().deleteAllCookies(); // reset IndexDB
//		driver.manage().window().setSize(new Dimension(800,480));
		
		hashCode = driver.hashCode();
		
		System.out.println(String.format("Der Driver mit dem Hash |%s| wurde initialisiert.", hashCode));
		
		return driver;
	}
	
	private final static void setFirefoxExePath(final String path) {
		TestController.firefoxExePath = path;
	}
	
	private final static void setFirefoxDriverPath(final String path) {
		TestController.firefoxDriverPath = path;
	}
	
	private final static void setChromeExePath(final String path) {
		TestController.chromeExePath = path;
	}
	
	private final static void setChromeDriverPath(final String path) {
		TestController.chromeDriverPath = path;
	}
	
	private final static void fetchConfig() {		
		final Properties  prop= new Properties();
		final FileInputStream ip;
		try {
			ip= new FileInputStream(configFilePath);
			prop.load(ip);
			
			setAppUrl(prop.getProperty("appUrl"));
			setChromeDriverPath(prop.getProperty("chromeDriverPath"));
			setChromeExePath(prop.getProperty("chromeExePath"));
			setFirefoxDriverPath(prop.getProperty("firefoxDriverPath"));
			setFirefoxExePath(prop.getProperty("firefoxExePath"));
			
			ip.close();
		} catch (FileNotFoundException e) {
			final String searchedFolder = System.getProperty("user.dir").substring(0, System.getProperty("user.dir").lastIndexOf("\\")); // folder path above java project
			final String orientationFile = searchedFolder + "\\appConfigDemo.properties";
			
			System.err.println(String.format("Die Config-File konnte nicht gefunden werden! Es wurde gesucht nach: |%s| im Verzeich: |%s|. Bei Problemen kann man sich an folgender Datei orientieren: |%s|", configFilePath, searchedFolder, orientationFile));
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		} finally {
			
		}
	}
}
