package main;

import test.sportarchive.header.CheckAppName;
import test.sportarchive.header.ToggleFullscreen;
import test.sportarchive.header.desktop.SwitchViews;
import test.sportarchive.main.overviewView.CheckMemberlistVisible;
import test.wikipedia.WikipediaDemo;
import testEngine.TestController;

public class Main {
	
	public final static void main(String[] args) {
//		executeWikipediaDemo();
		
		TestController.getDriver("firefox");
		executeTests();
		TestController.closeDriver();
		
		TestController.getDriver("chrome");
		executeTests();
		TestController.closeDriver();
	}
	
	private final static void executeTests() {
		CheckAppName.execute();		
		CheckMemberlistVisible.execute();
		SwitchViews.execute();
		ToggleFullscreen.execute();
	}

	@SuppressWarnings("unused")
	private final static void executeWikipediaDemo() {
		TestController.setAppUrl("https://wikipedia.de");
		TestController.getDriver("firefox");
		WikipediaDemo.execute();
		TestController.closeDriver();
		
		TestController.getDriver("chrome");
		WikipediaDemo.execute();
		TestController.closeDriver();
	}
}
