describe('Testsuite for the Testscript "testscript.js"', function() {
    
    const parametersMemberlist = [
        {description:'Test adding a member with a valid, mannly, grownup, german, member (all contacts, no comment, with umlauts)', expectedResult:true, lastname:'Wöll', firstname:'Rüdiger', gender:'m', dateOfBirth:new Date('01/01/2000'), dateOfAccession:new Date('05/04/2020'), streetName:'Ringstraße', streetNumber:'34', postalCode:'56170', residence:'Bendorf', nationality:'GER', job:'Handwerker', mobileNumber:'01544886655', landlineNumber:'0262212345', email:'ersterRuediger@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:''},
        {description:'Test adding a member with a valid, mannly, grownup, german, member (all contacts, with comment)', expectedResult:true, lastname:'Rammel', firstname:'Arthur', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid lastname)', expectedResult:false, lastname: null, firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid firstname)', expectedResult:false, lastname:'Mustermann', firstname: null, gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid gender)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:345345, dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid date of birth)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:'not a birthdate', dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid date of accession)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: 'not a date object', streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid streetname)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:142345235, streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid streetnumber)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:29, postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid postalcode)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:124235, residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid residence)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:new Date('01/04/2020'), nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid nationality)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:4321, job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid job)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:654232, mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (no contact info)', expectedResult:false, lastname:'Spyfox', firstname: 'Fred', gender:'m', dateOfBirth:new Date('01/01/1990'), dateOfAccession: new Date('01/01/2015'), streetName:'Spionstraße', streetNumber:'1', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Spion', mobileNumber: null, landlineNumber:null, email:null, isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:''},
        {description:'Test failing to add a member with invalid data (invalid executivestatus)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:'ja', isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid trainerstatus)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:'nein', rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid rank)', expectedResult:false, lastname:'Mustermann', firstname: 'Max', gender:'m', dateOfBirth:new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:new Date('01/04/2020'), comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (member is too young)', expectedResult:false, lastname:'Mustermann', firstname:'Max', gender:'m', dateOfBirth: new Date((new Date().getMonth()+1) +'/'+new Date().getDate() + '/'+new Date().getFullYear()), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid executive status)', expectedResult:false, lastname:'Mustermann', firstname:'Max', gender:'m', dateOfBirth: new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:12345, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid trainer status)', expectedResult:false, lastname:'Mustermann', firstname:'Max', gender:'m', dateOfBirth: new Date('10/10/1970'), dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:'this is not a boolean', rank:'Fortgeschrittener', comment:'Insolvent'},
        {description:'Test failing to add a member with invalid data (invalid birthdate)', expectedResult:false, lastname:'Mustermann', firstname:'Max', gender:'m', dateOfBirth: '5423423werwet', dateOfAccession: new Date('01/04/2020'), streetName:'Ludwigstraße', streetNumber:'29', postalCode:'56626', residence:'Andernach', nationality:'GER', job:'Offizier', mobileNumber:'01544886699', landlineNumber:'0263254321', email:'arthurdeutsch@gmx.de', isExecutive:false, isInactive:false, isTrainer:false, rank:'Fortgeschrittener', comment:'Insolvent'}
    ];
    
    beforeEach(function() {
        model.initMemberList();  
    });

    parametersMemberlist.forEach((currentValues) => {
        it(currentValues.description, function() {
            // lokale Variablen
            var currentResult

            // Verarbeitung
            currentResult = model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
            expect(currentValues.expectedResult).toEqual(currentResult);
        });
    });
    
    it('Test the initialisation of an already filled memberlist.', function() {
        // lokale Variablen
        var expectedResult = 0;
        var currentValues;
        var currentResult;

        // Verarbeitung
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        model.initMemberList();
        currentResult = model.getMemberList().size;

        expect(expectedResult).toEqual(currentResult);
    });

    it('Test the addition of a member to the memberlist.', function() {
        // lokale Variablen
        var expectedResult = 1;
        var currentValues;
        var currentResult;

        // Verarbeitung
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        currentResult = model.getMemberList().size;

        expect(expectedResult).toEqual(currentResult);
    });


    it('Test to get the memberlist with an initially filled list.', function() {
        // lokale Variablen
        var expectedSize = 2;
        var expectedLastname = parametersMemberlist[1].lastname;
        var currentValues;
        var currentSize;
        var currentLastname;

        // Verarbeitung
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        currentSize = model.getMemberList().size;
        currentLastname = model.getMemberList().get(2).lastname;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedLastname).toEqual(currentLastname);
    });

    it('Test changing a member in the memberlist with an initially filled list.', function() {
        var currentMember, currentResult, currentResultLastname;
        const memberId = 1;
        const expectedResult = true;
        const expectedResultLastname = 'Müller';
        
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        
        currentMember = model.getMemberList().get(memberId);
        currentMember.lastname = expectedResultLastname;

        currentResult = model.changeMember(memberId, currentMember);
        currentResultLastname = model.getMemberList().get(memberId).lastname;
        
        expect(expectedResult).toEqual(currentResult);
        expect(expectedResultLastname).toEqual(currentResultLastname);
    });
  
    it('Test deleting a member on deadline', function() {
        var currentMember, currentResult, currentListLength, currentContributionListLength;
        const memberId = 1;
        const expectedResult = true;
        const dateOfLeaving = new Date((new Date().getMonth()+1) +'/'+new Date().getDate() + '/'+ (new Date().getFullYear()-model.getDeleteYear()));
        const expectedListLength = 1;

        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        currentMember = model.getMemberList().get(memberId);
        currentMember.dateOfLeaving = dateOfLeaving;

        currentResult = model.changeMember(memberId, currentMember);
        
        currentResult = model.deleteMember();
        currentListLength = model.getMemberList().size;
        currentContributionListLength = model.getContributionsList().size;

        expect(expectedResult).toEqual(currentResult);
        //TODO look for a way to get the error message when Test fails
        expect(expectedListLength).toEqual(currentListLength);
        expect(expectedListLength).toEqual(currentContributionListLength);
    });

    it('Test not deleting a member when the deadline has not yet been met by one day', function() {
        var currentMember, currentResult, currentListLength, currentContributionListLength;
        const memberId = 1;
        const expectedResult = true;
        const dateOfLeaving = new Date((new Date().getMonth()+1) +'/'+new Date().getDate() + '/'+ (new Date().getFullYear()-model.getDeleteYear()));
        dateOfLeaving.setDate( dateOfLeaving.getDate() + 1);
        const expectedListLength = 2;
        
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        currentMember = model.getMemberList().get(memberId);
        currentMember.dateOfLeaving = dateOfLeaving;

        currentResult = model.changeMember(memberId, currentMember);
        
        currentResult = model.deleteMember();
        currentListLength = model.getMemberList().size;
        currentContributionListLength = model.getContributionsList().size;

        expect(expectedResult).toEqual(currentResult);
        d= new Date() ;
        //TODO look for a way to get the error message when Test fails
        expect(expectedListLength).toEqual(currentListLength);
        expect(expectedListLength).toEqual(currentContributionListLength);
    });

    it('Test getting the structure', function() {
        var currentResult;
        const expectedResultMale = [0,0,0,1,0,1,0];
        const expectedResultFemale = [0,0,0,0,0,0,0];
        const expectedResultDiverse = [0,0,0,0,0,0,0];
        
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        currentResult = model.getStructure();

        expect(expectedResultMale).toEqual(currentResult['m']);
        expect(expectedResultFemale).toEqual(currentResult['f']);
        expect(expectedResultDiverse).toEqual(currentResult['d']);
    });

    it('Test importing a lot of members', function() {
        var currentResult, expectedListLength;
        var expectedSize = 2;
        var expectedLastname = parametersMemberlist[1].lastname;
        var currentSize;
        var currentLastname;
        var listOfMembers = new Map();

        member1 = parametersMemberlist[0];
        member2 = parametersMemberlist[1];

        //the id for the entries in listOfMembers is not important
        listOfMembers.set(1, member1);
        listOfMembers.set(2, member2);
        model.addManyMembers(listOfMembers);
        expectedListLength = model.getMemberList().size;

        currentResult = model.getStructure();
        currentSize = model.getMemberList().size;
        currentLastname = model.getMemberList().get(2).lastname;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedLastname).toEqual(currentLastname);
    });

    it('Test the explanation for payment keys', function() {
        var expectedResult = new Map();
        expectedResult.set(1, 'Not a member')
        expectedResult.set(2, 'Full age member')
        expectedResult.set(3, 'Underage member')
        expectedResult.set(4, 'Trainer and/or CEO')
        expectedResult.set(5, 'Cancelled membership')
        expectedResult.set(6, 'Inactive')

        currentResult = model.getPaymentExplanation();
        
        expect(expectedResult).toEqual(currentResult);
    });

    it('Test setting the paymentstatus', function() {
        var paymentArray, year;
        year = 2019;
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        paymentArray = [0,1,1,1,2,2,2,1,1,1,1,1];
        model.setPaymentStatus(1,year,paymentArray);
        
        expect(paymentArray).toEqual(model.getContributionsList().get(1).paymentStatus.get(year));
    });

    it('Test getting the contribution list and checking the Maturity', function() {
        // lokale Variablen
        var expectedSize = 2;
        var expectedIsOfAge = true;
        var currentValues;
        var currentSize;
        var currentIsOfAge;

        // Verarbeitung
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);

        currentSize = model.getContributionsList().size;
        currentIsOfAge = model.getContributionsList().get(2).isOfAge;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedIsOfAge).toEqual(currentIsOfAge);
    });
    
    
    it('Test saving and loading memberList with an single member', async function() {
        // lokale Variablen
        const expectedSize = 1;
        const expectedLastname = parametersMemberlist[1].lastname; // from 2nd member
        
        var currentValues;
        var currentSize;
        var currentLastname;

        // Verarbeitung
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        await model.updateMemberList();
        
        await model.loadMemberList();   
        currentSize = model.getMemberList().size;
        currentLastname = model.getMemberList().get(1).lastname;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedLastname).toEqual(currentLastname);

    });

    it('Test saving and loading the memberList with multiple members', async function() {
        // lokale Variablen
        const expectedSize = 2;
        const expectedLastnameFirst = parametersMemberlist[0].lastname;
        const expectedLastnameSecond = parametersMemberlist[1].lastname;
        
        var currentValues;
        var currentSize;
        var currentLastname;

        // Verarbeitung
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        await model.updateMemberList();

        await model.loadMemberList();      
        currentSize = model.getMemberList().size;
        currentLastname = model.getMemberList().get(1).lastname;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedLastnameFirst).toEqual(currentLastname);

        currentLastname = model.getMemberList().get(2).lastname;
        expect(expectedLastnameSecond).toEqual(currentLastname);

    });
    
    it('Test saving and loading contributionList with an single member', async function() {
        // lokale Variablen
        const expectedSize = 1;
        const expectedIsOfAge = true; 
        
        var currentValues;
        var currentSize;
        var currentIsOfAge;

        // Verarbeitung
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        await model.updateContributionList();
        
        await model.loadContributionList();   
        
        currentSize = model.getContributionsList().size;
        currentIsOfAge = model.getContributionsList().get(1).isOfAge;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedIsOfAge).toEqual(currentIsOfAge);
    });

    it('Test saving and loading the contributionList with multiple members', async function() {
        // lokale Variablen
        const expectedSize = 2;
        const expectedIsOfAge1 = true; 
        const expectedIsOfAge2 = true; 
        
        var currentValues;
        var currentSize;
        var currentIsOfAge1;
        var currentIsOfAge2;

        // Verarbeitung
        currentValues = parametersMemberlist[0];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        currentValues = parametersMemberlist[1];
        model.addMember(currentValues.lastname, currentValues.firstname, currentValues.gender, currentValues.dateOfBirth, currentValues.dateOfAccession, currentValues.streetName, currentValues.streetNumber, currentValues.postalCode, currentValues.residence, currentValues.nationality, currentValues.job, currentValues.mobileNumber, currentValues.landlineNumber, currentValues.email, currentValues.isExecutive, currentValues.isTrainer, currentValues.rank, currentValues.comment);
        await model.updateContributionList();

        await model.loadContributionList();   
        currentSize = model.getContributionsList().size;
        currentIsOfAge1 = model.getContributionsList().get(1).isOfAge;
        currentIsOfAge2 = model.getContributionsList().get(2).isOfAge;

        expect(expectedSize).toEqual(currentSize);
        expect(expectedIsOfAge1).toEqual(currentIsOfAge1);
        expect(expectedIsOfAge2).toEqual(currentIsOfAge2);
    });
     
});